import MainMachine from './MainMachine';
import UserRegistrationMachine from './UserRegistrationMachine';

export default {
    MainMachine,
    UserRegistrationMachine
};