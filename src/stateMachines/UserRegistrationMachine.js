import Machine from "javascript-state-machine";
import {
    requestUserRole,
    setUserRole,
    requestFirstName,
    requestLastName,
    requestEmail,
    requestPhoneNumber,
    requestPassword,
    requestPasswordConfirmation,
    validateRegistrationInput,
    whoAmI,
    requestTopics,
    chooseTopic,
    topicsSelected,
    requestDiplomas,
    requestProfilePicture,
} from '../signUpController';

export default Machine.factory({
    init: 'initial',
    transitions: [
        {name: 'start', from: 'initial', to: 'role'},
        {name: 'studentOrTeacher', from: 'role', to: 'firstName'},
        {name: 'userInput', from: 'firstName', to: 'lastName'},
        {name: 'userInput', from: 'lastName', to: 'email'},
        {name: 'userInput', from: 'email', to: 'phoneNumber'},
        {name: 'userInput', from: 'phoneNumber', to: whoAmI},
        {name: 'userInput', from: 'topics', to: 'topicsAgain'},
        {name: 'userInput', from: 'topicsAgain', to: 'topics'},
        {name: 'chooseTopic', from: 'topics', to: 'diplomas'},
        {name: 'topicsSelected', from: 'topics', to: 'diplomas'},
        {name: 'topicsSelected', from: 'diplomas', to: 'diplomas'},
        // {name: 'userInput', from: 'diplomas', to: finishDiplomas},
        {name: 'userInput', from: 'diplomas', to: 'profilePicture'},
        // {name: 'userInput', from: 'profilePicture', to: finishProfilePicture},
        {name: 'userInput', from: 'profilePicture', to: 'password'},
        {name: 'userInput', from: 'password', to: 'passwordConfirmation'},
        {name: 'userInput', from: 'passwordConfirmation', to: 'password'},
        // {name: "noProfilePicture", from: 'profile_picture', to: 'video_presentation'},
        // {name: "hasProfilePicture", from: 'profile_picture', to: 'upload_profile_picture'},
        // {name: 'userInput', from: 'upload_profile_picture', to: 'video_presentation'},
        // {name: 'noVideoPresentation', from: 'video_presentation', to: 'upload_video_presentation'},
        // {name: 'hasVideoPresentation', from: 'video_presentation', to: 'upload_video_presentation'},
        // {name: 'userInput', from: 'upload_video_presentation', to: 'diplomas'},
        // {name: 'userInput', from: '*', to: 'diplomas'},
        {name: 'goto', from: '*', to: s => s},
    ],

    //constructor - sets user to the machine
    data: (user) => ({user: user}),

    methods: {
        onRole: requestUserRole,
        onLeaveRole: setUserRole,
        onFirstName: requestFirstName,
        onLeaveFirstName: validateRegistrationInput,
        onLastName: requestLastName,
        onLeaveLastName: validateRegistrationInput,
        onEmail: requestEmail,
        onLeaveEmail: validateRegistrationInput,
        onPhoneNumber: requestPhoneNumber,
        onLeavePhoneNumber: validateRegistrationInput,
        // onBeforeTopics: requestTopics,
        onEnterTopics: requestTopics,
        onLeaveTopics: requestTopics,
        onLeaveTopicsAgain: requestTopics,
        onTopicsAgain: requestTopics,
        onBeforeChooseTopic: chooseTopic,
        // onTopicsSelected: topicsSelected,
        onDiplomas: requestDiplomas,
        onLeaveDiplomas: validateRegistrationInput,
        // onDiplomasAgain: requestDiplomas,
        // onLeaveDiplomasAgain: validateRegistrationInput,
        onProfilePicture: requestProfilePicture,
        onLeaveProfilePicture: validateRegistrationInput,
        onPassword: requestPassword,
        onLeavePassword: validateRegistrationInput,
        onPasswordConfirmation: requestPasswordConfirmation,
        onLeavePasswordConfirmation: validateRegistrationInput,
        onPendingTransition: (transition, from, to) => {
            console.info("onPendingTransition", transition);
            throw new Error(`transition already in progress ${transition}:${from}=>${to}`);
        },
        onBeforeTransition: ({transition, from, to}) => (console.info(">>>>>onBeforeTransition",
            {transition, from, to})),
        onTransition: ({fsm, transition, from, to}) => {
            console.info(">>>>>onTransition",
                {transition, from, to});
            return fsm.transitionState !== false;
        },
        onEnterState: ({transition, from, to}) => (console.info(">>>>>onEnterState",
            {transition, from, to})),
        onAfterTransition:  ({transition, from, to}) => (console.info(">>>>>onAfterTransition",
            {transition, from, to})),
        onInvalidTransition(transition, from, to) {
            throw new Error(`transition is invalid in current state
             ${transition}:${from}=>${to} ${this.state}`);
        },
    }
});
