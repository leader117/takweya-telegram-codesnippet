'use strict';
import Machine from 'javascript-state-machine';

import {setLanguage, showLanguageMenu} from '../languageController';
import {showDashboard,underConstruction} from '../dashboard';
import {
    signInPasswordRequest,
    showMenuRegisterOrEnter,
    authenticateUser,
    checkUserRegistration,
    sendRegisteredMessage
} from '../signInController';

export default Machine.factory({
    init: 'initial',
    transitions: [
        {name: 'start', from: '*', to: 'language_choosing'},
        {name: 'authorized', from: ['initial', 'checking_password', 'authorization_check'], to: 'dashboard'},
        {name: 'setLanguage', from: 'language_choosing', to: "signing_in"},
        {name: 'setLanguage', from: 'initial', to: "signing_in"},
        {name: 'setLanguage', from: 'signing_in', to: "signing_in"},
        {name: 'register', from: '*', to: "user_registration"},
        {name: 'registered', from: 'user_registration', to: "signing_in"},
        {name: 'logout', from: '*', to: 'signing_in'},
        {name: 'login', from: '*', to: 'request_password'},
        {name: 'userInput', from: 'request_password', to: "dashboard"},
        {name: 'userInput', from: ['initial', 'language_choosing', 'user_registration'], to: "signing_in"},
        {name: 'underConstruction', from: '*', to: "dashboard"},
        {name: 'goto', from: '*', to: s => s},
    ],

    //constructor - sets user to the machine
    data: (user) => ({user: user}),

    methods: {
        onRegister: checkUserRegistration,
        onBeforeRegistered: sendRegisteredMessage,
        onLanguageChoosing: showLanguageMenu,
        onBeforeSetLanguage: setLanguage,
        onSigningIn: showMenuRegisterOrEnter,
        onLogin: signInPasswordRequest,
        onLeaveRequestPassword: authenticateUser,
        onDashboard: showDashboard,
        onUnderConstruction: underConstruction,
        onPendingTransition: function (transition, from, to) {
            throw new Error(`transition already in progress ${transition}:${from}=>${to}`);
        },
        onBeforeTransition: ({transition, from, to}) => (console.info(">>>>>onBeforeTransition",
            {transition, from, to})),
        onTransition: ({fsm, transition, from, to}) => {
            console.info(">>>>>onTransition",
                {transition, from, to});
            return fsm.transitionState !== false;
        },
        onEnterState: ({transition, from, to}) => (console.info(">>>>>onEnterState",
            {transition, from, to})),
        onAfterTransition: ({transition, from, to}) => (console.info(">>>>>onAfterTransition",
            {transition, from, to})),
        onInvalidTransition(transition, from, to) {
            throw new Error(`transition is invalid in current state ${transition}:${from}=>${to} ${this.state}`);
        },
    },
});