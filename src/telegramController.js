import TelegramBot from 'node-telegram-bot-api';

import {TELEGRAM_TOKEN} from '../config';

// const proxy = "socks://127.0.0.1:1090";

const bot = new TelegramBot(TELEGRAM_TOKEN, {
    // request: {
    //     proxy: 'http://127.0.0.1:1087'
    // }
});


async function sendMessageToTelegram(userId, message, format) {
    if (userId === undefined
        || userId === false
        || !message) return;
    console.info("sendMessageToTelegram", userId, message);

    let res;
    try {
        res = await bot.sendMessage(userId, message, format);
    } catch (err) {
        console.error("sendMessageToTelegram ERROR", err);
        return err;
    }

    console.info("sendMessageToTelegram sent", res);
    return true;
}

async function showTelegramMenuFromArray(userId, header, arrayOfKeyValueToShow, format) {
    console.info("showTelegramMenuFromArray:", userId, header, arrayOfKeyValueToShow,format);
    // console.info("userId", userId, "header", header, "arrayOfKeyValueToShow", arrayOfKeyValueToShow,
    //     "TELEGRAM_TOKEN", TELEGRAM_TOKEN, "bot", bot);
    if (!arrayOfKeyValueToShow || typeof arrayOfKeyValueToShow !== 'object'
        || !Object.keys(arrayOfKeyValueToShow).length) {
        console.warn("showTelegramMenuFromArray Wrong menu array to send", arrayOfKeyValueToShow);
        return;
    }

    if (Array.isArray(arrayOfKeyValueToShow) && Array.isArray(arrayOfKeyValueToShow[0])){
        arrayOfKeyValueToShow.forEach(rowOfButtons => {
            if(Array.isArray(rowOfButtons)){
                rowOfButtons.forEach(button => {
                    Object.keys(button).forEach(buttonTitle => {
                        //{ 'English': '/setLanguage English'}
                        button.text = buttonTitle;
                        button.callback_data = button[buttonTitle];
                        delete button[buttonTitle];
                    });
                });
            }
        });
        // Object.keys(arrayOfKeyValueToShow).forEach(menuItem => {
        //     console.info("creating button for", menuItem);
        //     markup.reply_markup.inline_keyboard.push(
        //         [{text: menuItem, callback_data: arrayOfKeyValueToShow[menuItem]}]);
        // });
        console.info("showTelegramMenuFromArray converted arr of arr", arrayOfKeyValueToShow);
    } else {
        //convert for telegram format
        let menuArray = [];
        Object.keys(arrayOfKeyValueToShow).forEach(key => {
            menuArray.push([{text: key, callback_data: arrayOfKeyValueToShow[key]}]);
        });
        arrayOfKeyValueToShow = menuArray;
        console.info("showTelegramMenuFromArray converted arr", arrayOfKeyValueToShow);
    }

    if (!header || typeof header !== 'string') {
        console.warn("showTelegramMenuFromArray wrong header", header);
        return;
    }

    const markup = Object.assign({},
        {reply_markup: {inline_keyboard: []}},
        format);

    markup.reply_markup.inline_keyboard = arrayOfKeyValueToShow;

    // Object.keys(arrayOfKeyValueToShow).forEach(menuItem => {
    //     console.info("creating button for", menuItem);
    //     markup.reply_markup.inline_keyboard.push(
    //         [{text: menuItem, callback_data: arrayOfKeyValueToShow[menuItem]}]);
    // });

    console.info("showTelegramMenuFromArray markup", markup);

    let res;
    try {
        res = await bot.sendMessage(userId, header, markup);
    } catch (err) {
        console.error(err);
        return err;
    }

    console.info("showTelegramMenuFromArray sent", res);
    return true;
}

function getFileFromTelegram(fileId) {
    return new Promise((resolve, reject)=> {
        const buffer = [];
        const fileStream = bot.getFileStream(fileId);
        fileStream.on('data', (data) => buffer.push(data));

        fileStream.on('end', () => {
            return resolve(data);
        });

        fileStream.on('error', err => {
            console.error("getFileFromTelegram",fileId,"error",err);
            reject(err);
        });
    });
}

async function getFileDataFromTelegram(fileId) {
    console.info("getFileDataFromTelegram", fileId);
    return await bot.getFile(fileId);
}

export {
    showTelegramMenuFromArray,
    sendMessageToTelegram,
    getFileFromTelegram,
    getFileDataFromTelegram
}