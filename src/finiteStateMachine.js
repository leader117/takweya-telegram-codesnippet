'use strict';
import {getUser, saveUserState, createNewUser, clearUser} from './accountController';
import MachineFactories from './stateMachines/MachineFactories';
import {sendMessageToTelegram} from "./telegramController";
import {loadTranslation} from "./languageController";

//this object for machines storing is accessible within one AWS Lambda container => can drop at any moment
const machines = {};

const namespace = "default";

function checkUserState(user) {
    if (!user.state) {
        return false;
    } else if (user.state.subState) {
        return user.state.subState;
    } else {
        return user.state.state;
    }
}

function checkUsersubMachine(user) {
    //if there's an empty subState then it will wreck everything
    if (user.state && user.state.subState) {
        return user.state.subMachine;
    }

    return false;
}

async function getUserAndStates(userId) {
    const result = {};

    result.user = await getUser(userId);

    if (result.user) {
        console.info("found existing user, checking state");
        //if user found just save the state to a var
        //to use it in GOTO of state machine after its creation
        result.savedState = checkUserState(result.user);

        console.info("found existing user, checking language");
        //load user's language translation if not loaded
        if (result.user.language) await loadTranslation(result.user.language);

        console.info("found existing user, checking submachine");
        //if user have a subMachine - use it instead of main
        result.subMachine = checkUsersubMachine(result.user);

        console.info("found existing user",
            result.user, result.savedState, result.subMachine);
    } else {


        ////////////////////////////////////
        //absolutely new user
        console.info("creating new User", result.user);
        result.user = await createNewUser(userId);
        console.info("created new User", result.user);
    }

    return result;
}

function recreateSubMachine(user, subMachine) {
    const userId = user.userId;
    if (subMachine) {
        console.info("got subMachine", subMachine, "for", user);
        machines[userId].activeMachine = subMachine;
        machines[userId][subMachine] = new MachineFactories[subMachine](user);
        console.info("created subMachine", subMachine, "for", user);
        return subMachine;
    } else {
        machines[userId].activeMachine = "MainMachine";
    }
}

async function createNewStateMachineForUser(userId) {
    if (machines[userId] && machines[userId]['MainMachine']) {
        console.info("Found user machine", userId);
        return machines[userId].activeMachine;
    }

    console.info("createNewStateMachineForUser started", userId);
    machines[userId] = {};

    let {user, savedState, subMachine} = await getUserAndStates(userId);

    console.info("creating new MainMachine for", userId);
    //creating new state machine
    machines[userId]["MainMachine"] = new MachineFactories["MainMachine"](user);
    console.info("created MainMachine for", userId);

    //if this is an existing user with subMachine
    //we need to create it
    recreateSubMachine(user, subMachine);

    const currentMachine = machines[userId].activeMachine;

    //finally we need to move the machine to the saved user state
    //to start the next action from it
    if (savedState) {
        console.info("moving machine", currentMachine,
            "to", savedState, "for the User", userId);
        await machines[userId][currentMachine].goto(savedState);
    }

    console.info("createNewStateMachineForUser finished", user);
    return currentMachine;
}

function createSubMachineForUser(user, subMachineName) {
    console.info("createSubMachineForUser started for user", user);
    machines[user.userId][subMachineName] = new MachineFactories[subMachineName](user);
    console.info("createSubMachineForUser ended for user", user);
    return machines[user.userId][subMachineName];
}

async function resetTheBotForUser(userId) {
    console.info("resetTheBotForUser", userId,
        machines[userId].activeMachine);

    let currentMachine = machines[userId].activeMachine;
    console.info("resetTheBotForUser machine:",
        machines[userId][currentMachine]);

    if(!currentMachine) {
        currentMachine = machines[userId].activeMachine = 'MainMachine';
    }

    if (!machines[userId][currentMachine].user) {
        console.error("User on the machine is null");
        return false;
    }

    //erase user submachine state
    if (machines[userId][currentMachine].user.state
        && machines[userId][currentMachine].user.state.subState) {
        machines[userId][currentMachine].user.state.subState = undefined;
        machines[userId][currentMachine].user.state.subMachine = undefined;
        delete machines[userId][currentMachine];
        machines[userId].activeMachine = 'MainMachine';
    }

    machines[userId][currentMachine].goto('initial');
    if (machines[userId][currentMachine].user.language) {
        await machines[userId][currentMachine].user.say(
            "`To start from language choosing write `*/start*\n" +
            "`To completely erase user and start write `*/cleanstart*", namespace,
            {parse_mode: "markdown"});
    } else {
        await sendMessageToTelegram(userId,
            "`To start from language choosing write `*/start*\n" +
            "`To completely erase user and start write `*/cleanstart*", {parse_mode: 'markdown'});
    }
    return machines[userId].activeMachine;
}

export default async ({userId, action, payload}) => {
    //********DEBUG
    if (action === 'cleanstart') {
        let res;
        try {
            res = await clearUser(userId);
        } catch (err) {
            console.err("clearUser", err.message);
        }
        delete machines[userId];
        if (res) {
            await sendMessageToTelegram(userId,
                '*OLD USER REMOVED*', {parse_mode: 'markdown'});
        }

        action = 'start';
    }
    //************

    let currentMachine;

    currentMachine = await createNewStateMachineForUser(userId)

    if (!currentMachine) {
        console.error("currentMachine undefined");
        return false;
    } else {
        console.info("currentMachine is", currentMachine);
    }

    if (action === 'start') {
        await sendMessageToTelegram(userId,
            '*Starting...*', {parse_mode: 'markdown'});
        currentMachine = await resetTheBotForUser(userId,);

        if (currentMachine === false) {
            //something is wrong

        }
    }


    let userActiveStateMachine = machines[userId][currentMachine];

    console.info("starting action:", action, "on",
        currentMachine, "from state",
        machines[userId][currentMachine].state,
        "payload", payload);

    if (!userActiveStateMachine[action] ||
        typeof userActiveStateMachine[action] !== 'function') {
        console.warn(action, 'is not a valid machine transition',
            userActiveStateMachine.allTransitions(), userActiveStateMachine);
        return false;
    }

    //turn on transitions by default
    //set to false if we need to stop transition
    userActiveStateMachine.transitionState = true;

    const res = await userActiveStateMachine[action](payload);

    console.info(currentMachine, "ending state",
        userActiveStateMachine.state, "res:", res, typeof res);

    if (res && typeof res === 'object'
        && typeof res.switchToSubmachine === 'string') {

        //***temporary for testing
        console.info(userId, "DEBUG saving because we got", res);
        await saveUserState(userActiveStateMachine);
        console.info("temporary saved");
        //***
        const subMachine = createSubMachineForUser(userActiveStateMachine.user,
            res.switchToSubmachine);

        machines[userId].activeMachine = res.switchToSubmachine;

        console.info("starting subMachine", res.switchToSubmachine);

        const subResponse = await subMachine.start();
        userActiveStateMachine.user.state.subMachine = res.switchToSubmachine;
        userActiveStateMachine.user.state.subState = subMachine.state;

        console.info("started subMachine", res.switchToSubmachine,
            "state=>", subMachine.state, "subResponse", subResponse);
    }

    if (currentMachine !== 'MainMachine') {
        //we were running a submachine for this iteration
        if (currentMachine === 'UserRegistrationMachine'
            && userActiveStateMachine.user.isRegistered) {
            //this submachine is finished
            userActiveStateMachine.user.state.subMachine = undefined;
            userActiveStateMachine.user.state.subState = undefined;
            delete machines[userId][currentMachine];
            console.info(userId, 'deleted submachine',
                currentMachine,
                machines[userId][currentMachine]);

            //and we must continue MainMachine.
            machines[userId].activeMachine = "MainMachine";
            userActiveStateMachine = machines[userId]["MainMachine"];
            await userActiveStateMachine.goto('user_registration');
            await userActiveStateMachine['registered']();
        } else {
            userActiveStateMachine.user.state.subMachine = currentMachine;
            userActiveStateMachine.user.state.subState
                = userActiveStateMachine.state;
        }
    }

    await saveUserState(userActiveStateMachine);

    return userActiveStateMachine;
};
