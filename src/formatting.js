function addActionToEveryArrayItem(arrayOfItems, action) {
    console.info("addActionToEveryArrayItem", arrayOfItems, action);

    let objectWithActions = arrayOfItems.reduce((obj, item) => (obj[item]
        = `${action} "${item}"`, obj), {});

    console.info("addActionToEveryArrayItem added actions:",
        objectWithActions);
    //we split array by 5 items in a column
    const result = chunkObjectIntoArrayOfArrays(objectWithActions, 3);

    console.info("addActionToEveryArrayItem splitted:", result);

    return result;
}

function chunkObjectIntoArrayOfArrays(obj, size) {
    console.info("chunkObjectInArrayGroups:", Object.keys(obj).length, size);
    const final = [];
    let counter = 0;
    let portion = [];

    const numberOfItemsInArow = Math.ceil(Object.keys(obj).length / size);

    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            if (counter !== 0 && counter % numberOfItemsInArow === 0) {
                console.info("portion:", portion);
                final.push(portion);
                portion = [];
            }
            portion.push({[key]: obj[key]});
            counter++
        }
    }
    final.push(portion);

    console.info("chunkObjectIntoArrayOfArrays result:", final);
    return final;
}

export {
    addActionToEveryArrayItem
}