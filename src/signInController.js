'use strict';
const namespace = 'signIn';

async function authenticateUser({fsm}, password) {
    console.info("authenticating user with", fsm, 'password:', password);
    if (!password) {
        fsm.transitionState = false;
        return false;
    }

    let passwordValidation;

    try {
        passwordValidation = await fsm.user.comparePassword(password);
    } catch (err) {
    }

    if (passwordValidation) {
        console.info("Password confirmed");
    } else {
        fsm.transitionState = false;
        await fsm.user.say('Password is wrong', namespace);
        return false;
    }

    await fsm.user.say('Password correct', namespace);
    return true;
}

async function checkUserRegistration({fsm}) {
    if (fsm.user.isRegistered) {
        fsm.transitionState = false;

        return false;
    }

    return {switchToSubmachine: "UserRegistrationMachine"};
}

async function sendRegisteredMessage({fsm}) {
    console.info("sendRegisteredMessage", fsm);
    await fsm.user.say(
        "Registration is complete - you can login with your password",
        namespace);

    return true;
}

async function showMenuRegisterOrEnter({fsm}) {
    const registerText = fsm.user.translate('Registration', namespace);
    const enterText = fsm.user.translate('Enter with the password', namespace);

    const registerOrEnterMenu = {};
    registerOrEnterMenu[registerText] = "/register";
    registerOrEnterMenu[enterText] = "/login";

    console.info("showMenuRegisterOrEnter registerOrEnterMenu", registerOrEnterMenu);

    await fsm.user.sendMenu("Please make a choice", registerOrEnterMenu, namespace);

    return true;
}

async function signInPasswordRequest({fsm}) {

    if(fsm.user.roles.teacher && fsm.user.isVerified === false){
        await fsm.user.say(
            "Dear teacher you are NOT VERIFIED. But you can proceed as a test", namespace);
    }

    await fsm.user.say(
        "title", namespace);

    return true;
}

function getAuthorizationStatus() {

    if (!this.user.state || !this.user.state.state) {
        //absolutely new user
        console.info("getAuthorizationStatus for ", this.user.userId,
            "language_choosing");
        return 'language_choosing';
    }

    if (this.user.language) {
        console.info("getAuthorizationStatus for ", this.user.userId,
            "signing_in");
        return 'signing_in';
    }

}


export {
    signInPasswordRequest,
    showMenuRegisterOrEnter,
    getAuthorizationStatus,
    authenticateUser,
    sendRegisteredMessage,
    checkUserRegistration
}