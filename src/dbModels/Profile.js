import * as mongoose from "mongoose";

const ProfileShema = new mongoose.Schema({
    userId: {type: Number, required: true, unique: true},
    telegramUsername: {type: String},
    profilePicture: {type: mongoose.Schema.Types.ObjectId, ref: 'picture'},
    profileVideo: {type: mongoose.Schema.Types.ObjectId, ref: 'video'},
    documents: [{type: mongoose.Schema.Types.ObjectId, ref: 'document'}],
    pictures: [{type: mongoose.Schema.Types.ObjectId, ref: 'picture'}],
    topics: [{type: mongoose.Schema.Types.ObjectId, ref: 'topic'}],
    firstName: {type: String},
    lastName: {type: String},
    phoneNumber: {type: String, unique: true},
    email: {type: String, unique: true},
    rating: {type: Number},
    notes: {type: String},
}, {collection: 'profiles', timestamps: true});

export default mongoose.model('profile', ProfileShema);