import * as mongoose from "mongoose";

const VideoShema = new mongoose.Schema({
    userId: {type: Number, required: true},
    video: {data: Buffer, contentType: String},
}, {collection: 'videos', timestamps: true});

export default mongoose.model('videos', VideoShema);
