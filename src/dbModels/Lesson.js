import * as mongoose from "mongoose";

const LessonShema = new mongoose.Schema({
    teacherId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    quesionId: {type: mongoose.Schema.Types.ObjectId, ref: 'Question'},
    startTime: {type: Date},
    endTime: {type: Date},
    rating: {type: Number},
    price: {type: Number},
}, {collection: 'lessons', timestamps: true});

export default mongoose.model('lesson', LessonShema);