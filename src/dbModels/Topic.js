import * as mongoose from "mongoose";

const TopicSchema = new mongoose.Schema({
    name: {type: String, required: true, unique: true},
}, {collection: 'topics', timestamps: false});

export default mongoose.model('topic', TopicSchema);
