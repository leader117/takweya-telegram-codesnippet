import * as mongoose from "mongoose";

const QuestionShema = new mongoose.Schema({
        userId: {type: Number, required: true},
        documents: [{type: mongoose.Schema.Types.ObjectId, ref: 'document'}],
        pictures: [{type: mongoose.Schema.Types.ObjectId, ref: 'picture'}],
        videos: [{type: mongoose.Schema.Types.ObjectId, ref: 'video'}],
        grade: {type: Number, required: true, default: 0},
    },
    {collection: 'questions', timestamps: true});

export default mongoose.model('question', QuestionShema);