import * as mongoose from "mongoose";

const PictureShema = new mongoose.Schema({
    data: String,
    contentType: String
}, {collection: 'pictures', timestamps: true});

export default mongoose.model('picture', PictureShema);