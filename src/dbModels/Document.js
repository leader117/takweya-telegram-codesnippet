import * as mongoose from "mongoose";

const DocumentShema = new mongoose.Schema({
    userId: {type: Number, required: true},
    text: {data: Buffer, contentType: String},
}, {collection: 'documents', timestamps: true});

export default mongoose.model('document', DocumentShema);
