import * as mongoose from "mongoose";

const I18translation = new mongoose.Schema({
    language: {type: String, required: true, unique: true},
    translation: {type: String, required: true, minlength: "10"}
}, { collection: 'i18n' });
// ^^^^^^^^^^^^^^^^^^^^^^^ needs to be added because mongoose has an old flaw
// with adding "s" to collection names

export default mongoose.model('i18n', I18translation);
