import * as mongoose from "mongoose";

const UserStateShema = new mongoose.Schema({
    userId: {type: Number, required: true, unique: true},
    state: {type: String},
    subState:  {type: String},
    subMachine: {type: String},
}, {collection: 'userStates', timestamps: true});

export default mongoose.model('userState', UserStateShema);