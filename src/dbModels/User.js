import * as mongoose from "mongoose";
import bcrypt from 'bcryptjs';
import i18next from 'i18next';
import {sendMessageToTelegram, showTelegramMenuFromArray} from "../telegramController";

const UserSchema = new mongoose.Schema({
    userId: {type: Number, required: true, unique: true},
    password: {type: String},
    language: {type: String},
    defaultGrade: {type: Number, default: 0},
    isVerified: {type: Boolean, default: false},
    isRegistered: {type: Boolean, default: false},
    roles: {
        student: {type: Boolean, default: false},
        teacher: {type: Boolean, default: false},
        admin: {type: Boolean, default: false},
    },
    profile: {type: mongoose.Schema.Types.ObjectId, ref: 'profile'},
    accountBalance: {type: mongoose.Schema.Types.ObjectId, ref: 'accountBalance'},
    state: {type: mongoose.Schema.Types.ObjectId, ref: 'userState'},
}, {collection: 'users', timestamps: true});

UserSchema.pre('save', function (next) {
    // only hash the password if it has been modified (or is new)
    if (!this.isModified('password')) {
        return next();
    }

    var user = this;

    // generate a salt
    bcrypt.genSalt(10, (error, salt) => {
        if (error) {
            return next(error);
        }

        // hash the password using the new salt
        bcrypt.hash(user.password, salt, function (error, hash) {
            if (error) {
                return next(error);
            }
            // override the cleartext password with the hashed one
            user.password = hash;
            return next();
        });
    });
});

UserSchema.methods.comparePassword = function (passw) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(passw, this.password, function (err, isMatch) {
            if (err) {
                return reject(err);
            }
            return resolve(isMatch);
        });
    });
};

UserSchema.methods.translate = function (message, namespace) {
    if (namespace) {
        message = `${namespace}:${message}`;
    }
    return i18next.t(message, {lng: this.language})
};

UserSchema.methods.say = async function (message, namespace, mode) {
    if (namespace && typeof namespace !== 'object') {
        message = `${namespace}:${message}`;
    } else {
        mode = namespace;
    }

    message = i18next.t(message, {lng: this.language});

    if (!mode) {
        mode = {parse_mode: 'markdown'};
    }
    message = "`" + message + "`";


    return await sendMessageToTelegram(this.userId,message
        , mode);
};

UserSchema.methods.sendMenu = async function (header, menu, namespace) {
    console.info("UserSchema sendMenu got", header, menu, namespace);
    //menu is an array of objects
    //every array item is a line
    //every array item's item is a column

    if (namespace) {
        header = `${namespace}:${header}`;
    }
    if (Array.isArray(menu) && Array.isArray(menu[0])) {
        menu.forEach(rowOfButtons => {
            if (Array.isArray(rowOfButtons)) {
                rowOfButtons.forEach(button => {
                    Object.keys(button).forEach(buttonTitle => {
                        //{ 'English': '/setLanguage English'}
                        //translate to user language
                        console.info("translating button:",
                            buttonTitle, "to language", this.language);
                        console.info("result",
                            i18next.t(buttonTitle, {lng: this.language}));
                        console.info(
                            "Translations test:",i18next.t("Islamic Studies"));
                        // console.info("bundle loaded?", i18next.hasResourceBundle(this.language,
                        //         "default"));
                        let buttonTitleTranslated = i18next.t(buttonTitle, {lng: this.language});
                        if (buttonTitleTranslated !== buttonTitle) {
                            button[buttonTitleTranslated] = button[buttonTitle];
                            delete button[buttonTitle];
                        }
                        console.info("result button", button);
                    });
                });
            }
        });
    }


    return await showTelegramMenuFromArray(this.userId,
        i18next.t(header, {lng: this.language}),
        menu, {parse_mode: "markdown"});
};


export default mongoose.model('user', UserSchema);
