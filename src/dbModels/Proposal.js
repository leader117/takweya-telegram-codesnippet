import * as mongoose from "mongoose";

const ProposalShema = new mongoose.Schema({
    userId:  {type: Number, required: true},
    questionId: {type: mongoose.Schema.Types.ObjectId, ref: 'question'},
    price: {type: Number, required: true},
    note: {type: String},
}, {collection: 'proposals', timestamps: true});

export default mongoose.model('proposal', ProposalShema);