import * as mongoose from "mongoose";

const AccountBalance = new mongoose.Schema({
    userId: {type: Number, required: true, unique: true},
    balance: {type: Number, default: 0},
}, {collection: 'accountBalances', timestamps: true});

export default mongoose.model('accountBalance', AccountBalance);