'use strict';
import {AccountBalance, Profile, User, UserState} from './dbModels/index';

async function getUserState() {
    let userState;

    try {
        userState = await UserState.findOne({userId: this.user.userId}, 'state')
            .select('-_id').exec();
        console.info(`got state for userId ${this.user.userId}:`, userState);
    } catch (err) {
        console.error(err);
    }

    return userState;
}

// async function getUserAndState(userId) {
//     let user;
//
//     try {
//         user = await User.findOne({userId: userId}).populate('state').exec();
//         if (user) console.info(`got user for userId ${User.userId}:`, user);
//     } catch (err) {
//         console.error(err);
//     }
//
//     return user;
// }

async function getUser(userId) {
    let user;
    console.info("searching for User", userId);

    try {
        user = await User.findOne({userId})
            .populate('state profile accountBalance').exec();
    } catch (err) {
        console.error(err);
    }

    if (user) {
        console.info(`got user for userId ${userId}:`, user);
    } else {
        console.info("user not found for", userId);
    }

    return user;
}

async function saveUserState(fsm) {
    console.info("saveUserState started with state", fsm.state);

    if (!fsm.user.state) return;

    if (!fsm.user.state.subState)
        fsm.user.state.state = fsm.state;

    if (fsm.state !== "none" && fsm.state !== "initial") {
        console.info("saving user state:", fsm.user.state);

        let res = await fsm.user.state.save();

        console.info(`saved state for ${fsm.user.userId} => ${fsm.state}`, res);
    }

    //*****DEBUG
    const res = await UserState.find({userId: fsm.user.userId});
    console.info("saved state:", res);
    //*****
    return true;
}

async function saveUser() {
    try {
        return await this.user.save();
    } catch (err) {
        console.warn(err);
        return false;
    }
}

async function createNewUser(userId) {
    const user = new User({userId});
    user.profile = new Profile({userId});
    user.accountBalance = new AccountBalance({userId});
    user.state = new UserState({userId});
    user.userId = userId;
    user.state.userId = userId;
    user.accountBalance.userId = userId;
    user.state.userId = userId;

    await user.save();
    await user.profile.save();
    await user.accountBalance.save();
    await user.state.save();

    console.info("created new user", user);
    return user;
}

async function clearUser(userId) {
    console.info("clearUser searching for", userId);
    try {
        await Profile.deleteOne({userId}).exec();
        console.info("clearUser profile removed", userId);
    } catch (err) {
        console.info(err.message);
    }
    try {
        await UserState.deleteOne({userId}).exec();
        console.info("clearUser state removed", userId);
    } catch (err) {
        console.info(err.message);

    }
    try {
        await AccountBalance.deleteOne({userId}).exec();
        console.info("clearUser accountBalance removed", userId);
    } catch (err) {
        console.info(err.message);

    }
    try {
        await User.deleteOne({userId}).exec();
        console.info("clearUser user removed", userId);
    } catch (err) {
        console.info(err.message);

    }

    return true;
}


export {
    getUserState,
    saveUserState,
    saveUser,
    getUser,
    createNewUser,
    clearUser
}