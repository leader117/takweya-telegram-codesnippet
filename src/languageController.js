'use strict';
import i18next from 'i18next';
import mongoose from 'mongoose';

import {addActionToEveryArrayItem} from './formatting';

import {I18translation} from './dbModels/index';
import {showTelegramMenuFromArray} from "./telegramController";

const namespace = "languageController";
let i18nextIsInitialized;

async function getLanguagesList() {
    let availableLanguages;
    console.info("mongoose.connection.readyState", mongoose.connection.readyState);

    try {
        availableLanguages = await I18translation.find({}, 'language')
            .select('-_id').exec();
        console.info("got availableLanguages", availableLanguages);
        if (typeof availableLanguages === 'object') {
            return availableLanguages.map(line => line.language);
        }
    } catch (err) {
        console.error(err);
    }
    return false;
}

async function setLanguage({fsm}, language) {
    console.info(`setLanguage: Changing language of ${fsm.userId} to ${language}`);
    await loadTranslation(language);

    fsm.user.language = language;

    try {
        await fsm.user.save();
    } catch (err) {
        console.error("mongoose.connection.readyState", mongoose.connection.readyState);
        console.error(err);
    }

    console.info("setLanguage: ", fsm.user.userId, "saved");
    return true;
}

function initI18n(){
    if (!i18nextIsInitialized) {
        return new Promise((resolve) => {
            i18next.init(
                {
                    fallbackLng: 'English',
                    defaultNS: 'default',
                    debug: true
                }, (err) => {
                    if (err) console.error('something went wrong loading i18n', err);
                    return resolve(true);
                });
            i18nextIsInitialized = true;
            console.info("i18next.isInitialized");
        })
    } else {
        console.info("reusing i18next.isInitialized");
        return true;
    }
}

async function loadTranslation(language) {
    console.info("loadTranslation",language);
    let translation;

    await initI18n();

    language = language.replace(/\"/g, "");

    //we don't need _id field
    if (i18next.hasResourceBundle(language, "default")) {
        console.info(language, "translation already loaded");
    } else {
        console.info(language, "language is loading",{language});
        try {
            const translationJSON = await I18translation.findOne({language})
                .select('-_id').exec();

            console.info("got translationJSON", translationJSON);
            translation = JSON.parse(translationJSON.translation);

            //load to i18next everything we got from DB
            Object.keys(translation).forEach(async (key) => {
                await i18next.addResourceBundle(language, key,
                    translation[key]);
                console.info(language, "loaded", key);
            });
        } catch (err) {
            console.error("error loading", language, err.message);
            return false;
        }

        console.info("Translations test:",
            i18next.t("Islamic Studies"),
            "bundle loaded?",i18next.hasResourceBundle(language, "default"));

        await i18next.loadNamespaces(Object.keys(translation));
    }

    return true;
}

async function showLanguageMenu() {
    console.info("entered showLanguageMenu");
    const availableLanguagesArray = await getLanguagesList();
    console.info("this.user.userId", this.user.userId);

    const availableLanguagesObject
        = addActionToEveryArrayItem(availableLanguagesArray, "/setLanguage");

    console.info("addActionToEveryArrayItem", availableLanguagesObject);

    //we can't send menu via user because he didn't chose his language
    await showTelegramMenuFromArray(this.user.userId,
        "Please choose your language",
        availableLanguagesObject);
    return true;
}

export {
    setLanguage,
    showLanguageMenu,
    loadTranslation
}