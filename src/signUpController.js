import {verifyUserData, sendUserForVerification} from './verificationController';
import {Topic, Picture} from './dbModels/index';
import {addActionToEveryArrayItem} from './formatting';
import {sendMessageToTelegram} from "./telegramController";

const namespace = "signUpController";

const requestUserRole = async ({fsm}) => {
    if (!fsm.user) {
        console.fatal("fsm user null", fsm);
        return false;
    }

    console.info("requestUserRole", fsm.user);

    const studentText = "Sign up as a Student";
    const teacherText = "Sign up as a Teacher";

    const studentOrTeacherMenu = {};
    studentOrTeacherMenu[studentText] = "/studentOrTeacher Student";
    studentOrTeacherMenu[teacherText] = "/studentOrTeacher Teacher";

    const res = await fsm.user.sendMenu("Are you a student or a teacher?",
        studentOrTeacherMenu, namespace);

    return res ? res : false;
};

const setUserRole = async ({fsm, from}, data) => {
    console.info("setUserRole", fsm.user.userId);
    if (data === "Student") {
        fsm.user.roles.student = true;
        await fsm.user.say(
            "Student - OK", namespace);
    } else {
        fsm.user.roles.teacher = true;
        await fsm.user.say(
            "Teacher - OK", namespace);
    }

    console.info("0saving user sign up data", fsm.user.userId);
    const res = await fsm.user.save();
    console.info("saved user sign up data", res);

    return res ? res : false;
};

const requestFirstName = async ({fsm, transition}) => {
    console.info("requestFirstName", fsm.user.userId);
    let res;
    if (transition !== "goto")
        res = await fsm.user.say(
            "Enter your first name", namespace);

    return res ? res : false;
};

const requestLastName = async ({fsm, transition}) => {
    console.info("requestLastName", fsm.user.userId);
    let res;
    if (transition !== "goto")
        res = await fsm.user.say(
            "Enter your last name", namespace);

    return res ? res : false;
};

const requestEmail = async ({fsm, transition}) => {
    console.info("requestEmail", fsm.user.userId);
    let res;
    if (transition !== "goto")
        res = await fsm.user.say(
            "Enter your Email address", namespace);

    return res ? res : false;

};

const requestPhoneNumber = async ({fsm, transition}) => {
    console.info("requestPhoneNumber", fsm.user.userId);
    let res;
    if (transition !== "goto")
        res = await fsm.user.say(
            "Enter your phone number", namespace);

    return res ? res : false;
};

const requestPassword = async ({fsm, from, transition}) => {
    // await sendMessageToTelegram(-1001479295242, "hello");
    if (from === 'profilePicture') {
        await fsm.user.say('Finished uploading profile picture',
            'signUp',
            {
                parse_mode: 'markdown',
                reply_markup: JSON.stringify({
                    keyboard: [],
                    remove_keyboard: true
                })
            });
        await sendMessageToTelegram('\n------------',
            'signUp',
            {
                parse_mode: 'markdown',
                reply_markup: {
                    reply_markup: JSON.stringify({
                        remove_keyboard: true
                    })
                }
            });
    }
    console.info("requestPassword", fsm.user.userId);
    let res;
    if (transition !== "goto")
        res = await fsm.user.say(
            "Enter your password", namespace);

    return res ? res : false;
};

const requestPasswordConfirmation = async ({fsm, transition}) => {
    console.info("requestPasswordConfirmation", fsm.user.userId);
    let res;
    if (transition !== "goto")
        res = await fsm.user.say(
            "Enter your password again", namespace);

    return res ? res : false;
};

const validateRegistrationInput = async ({fsm, from, to, transition}, data) => {
    console.info(`validateRegistrationInput ${transition}:${from}=>${to}`,
        `${fsm.user.userId}`, data);

    if (transition === 'goto') {
        console.info("validateRegistrationInput with goto transition - returning")
        return true;
    }

    if (from === 'initial') {
        if (to === 'diplomas') {
            from = 'diplomas';
        }
        if (to === 'profilePicture') {
            from = 'profilePicture';
        }
    }

    if (from === 'diplomas' || from === 'profilePicture') {
        if (data === fsm.user.translate('Finish')) {
            console.info("Got finish from user");
            return true;
        }
    }

    let verifyResult;
    verifyResult = await verifyUserData(from, data, fsm.user);

    console.info("validateRegistrationInput verifyResult", verifyResult);
    //if we got a message then something is wrong and we must tell it to user
    if (verifyResult) {

        await fsm.user.say(
            `${data} - ${verifyResult}`, namespace);

        //in case it's a password confirmation we must
        //return to password dialog
        if (from === 'passwordConfirmation') {
            return true;
        }

        fsm.transitionState = false;

        return false;
    }

    console.info("validateRegistrationInput switch", from);
    switch (from) {
        case 'password':
            fsm.user.password = data;
            break;
        case 'passwordConfirmation':
            //if it's a Student we end registration
            fsm.user.isRegistered = true;
            fsm.transitionState = false;
            if (fsm.user.teacher) {
                await fsm.user.say("Registration completed" +
                    " - please wait for verification");
                await sendUserForVerification(fsm.user);
            }
            return false;
        case 'firstName':
            fsm.user.profile.firstName = data;
            break;
        case 'lastName':
            fsm.user.profile.lastName = data;
            break;
        case 'email':
            fsm.user.profile.email = data;
            break;
        case 'phoneNumber':
            fsm.user.profile.phoneNumber = data;
            break;
        case 'diplomas':
            let diploma = new Picture({data: data.fileId, contentType: 'telegram'});
            fsm.user.profile.pictures.push(diploma);
            await diploma.save();
            await fsm.user.say('Picture received');
            fsm.transitionState = false;
            return false;
        case 'profilePicture':
            const picture = new Picture({data: data.fileId, contentType: 'telegram'});
            fsm.user.profprofilePicture = picture;
            await picture.save();
            await fsm.user.say('Profile picture received');
            fsm.transitionState = false;
            return false;
        default:
            break;
    }

    console.info("saving user sign up data", fsm.user.userId);
    let res = await fsm.user.profile.save();
    console.info("saved user profile data", res);
    res = await fsm.user.save();
    console.info("saved user sign up data", res);

    fsm.transitionState = true;
    return res ? res : false;
};

function whoAmI() {
    console.info("entering whoAmI", this.user.userId);
    return this.user.roles.teacher ? 'topics' : 'requestProfilePicture'
}


const requestTopics = async ({fsm, transition}) => {
    if (transition === 'topicsSelected') return;
    console.info("requestTopics sending");
    const topics = await getTopicsFromDbExcluding(fsm.user.profile.topics);
    if (topics) {
        await sendTopicsToChooseFrom(fsm.user, topics);
    }
};

const getTopicsFromDbExcluding = async (excludeTopics) => {
    try {

        const result = await Topic.find({_id: {$nin: excludeTopics}}).select('-_id');
        if (result) {
            console.info("Got topics without user's", result.map(obj => obj.name));
            return result.map(obj => obj.name);
        }
    } catch (err) {
        console.error(err);
    }
    return false;
};

const sendTopicsToChooseFrom = async (user, topics) => {
    console.info("entered sendTopicsToChooseFrom");

    const topicsObject
        = addActionToEveryArrayItem(topics, "/chooseTopic");

    addConfirmButtonToMenu(topicsObject, user.translate(
        "End selection"
    ), "/topicsSelected");

    await user.sendMenu("Please select a topic",
        topicsObject, namespace);

    return true;
};

function addConfirmButtonToMenu(topicsObject, text, action) {
    // const boldButtonText = `*${text}*`;
    topicsObject.push([{[text]: action}]);
}

const chooseTopic = async ({fsm}, topic) => {
    //get topic ID
    console.info("chooseTopic", {name: topic});
    //remove doublequotes from topic
    topic = topic.replace(/\"/g, "");
    const topicId = await Topic.findOne({name: topic}).select('_id').exec();
    console.info("chooseTopic got id", topicId, topic);
    if (!topicId) return false;

    if (fsm.user.profile.topics && fsm.user.profile.topics.indexOf(topicId) === -1) {
        fsm.user.profile.topics.push(topicId);
        console.info("chooseTopic fsm.user.profile.topics",
            fsm.user.profile.topics);
    }

    await fsm.user.say(topic);

    fsm.transitionState = false;
    return false;
};

const topicsSelected = async () => {

};

const requestDiplomas = async ({fsm}) => {
    console.info("requestDiplomas");
    const buttonText = fsm.user.translate('Finish');
    await fsm.user.say("Please attach photos of your qualification and click Finish when done",
        "signUp",
        {
            parse_mode: 'markdown',
            reply_markup: JSON.stringify({
                keyboard: [
                    [buttonText]
                ],
                resize_keyboard: true
            })
        });
};

function finishDiplomas(data) {
    if (data === this.user.translate('Finish')) {
        return 'profilePicture';
    }
    return 'diplomas';
}

function finishProfilePicture(data) {
    if (data === this.user.translate('Finish')) {
        return 'password';
    }
    return 'profilePicture';
}

const requestProfilePicture = async ({fsm, from}) => {
    if (from === 'diplomas') {
        await fsm.user.say('Finished uploading pictures');
    }
    console.info("requestProfilePicture");
    const buttonText = fsm.user.translate('Finish');
    await fsm.user.say("You can add a profile picture, click Finish when done",
        "signUp",
        {
            parse_mode: 'markdown',
            reply_markup: JSON.stringify({
                keyboard: [
                    [buttonText]
                ],
                resize_keyboard: true
            })
        });

};

export {
    requestUserRole,
    setUserRole,
    requestFirstName,
    requestLastName,
    requestEmail,
    requestPhoneNumber,
    requestPassword,
    requestTopics,
    requestPasswordConfirmation,
    validateRegistrationInput,
    whoAmI,
    chooseTopic,
    topicsSelected,
    requestDiplomas,
    finishDiplomas,
    requestProfilePicture,
    finishProfilePicture
}