const namespace = "dashboard";

const showDashboard = async ({fsm}) => {
    let text1, text2, text3, text4;
    const dashboardMenu = {};
    if (fsm.user.roles.teacher) {
        text1 = fsm.user.translate('My subscriptions', namespace);
        text2 = fsm.user.translate('Get the money', namespace);
    } else {
        text1 = fsm.user.translate('I need help', namespace);
        text2 = fsm.user.translate('Recharge the balance', namespace);

    }
    text3 = fsm.user.translate('My profile', namespace);
    text4 = fsm.user.translate('My history', namespace);
    const logoutText = fsm.user.translate('Log out', namespace);

    dashboardMenu[text1] = "/underConstruction";
    dashboardMenu[text2] = "/underConstruction";
    dashboardMenu[text3] = "/underConstruction";
    dashboardMenu[text4] = "/underConstruction";
    dashboardMenu[logoutText] = "/logout";

    console.info("showMenuRegisterOrEnter registerOrEnterMenu",
        dashboardMenu);

    await fsm.user.sendMenu("Dashboard", dashboardMenu, namespace);

    return true;
};

const underConstruction = async ({fsm}) => {
    await fsm.user.say("Coming soon!");
};

export {
    showDashboard,
    underConstruction
}