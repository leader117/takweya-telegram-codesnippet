'use strict';

function checkIfEventBodyIsJSON(event) {
    try {
        return JSON.parse(event.body);
    } catch (e) {
        return event.body;
    }
}

function wrongEvent(event) {
    if (!event || typeof event !== 'object') {
        console.info("event is not an object", typeof event, JSON.stringify(event));
        return true
    }
    if (!event.body && !event.message && !event.callback_query) {
        console.info("no body or message or callback_query in event", JSON.stringify(event));
        return true
    }

    return false;
}

function getUserIdFrom(event) {
    //if it is callback_query "message" object is within "callback_query" object
    //and real message is in "event.data" object
    if (event.callback_query) {
        event.callback_query.message.text = event.callback_query.data;
        event.message = event.callback_query.message;
    }

    //we take chat.id because it is == userID
    //we can't take message.from.id because in callback_query there's botID, not user
    if (!event.message || typeof event.message !== 'object'
        || !event.message.chat || typeof event.message.chat !== 'object'
        || !event.message.chat.id) {
        console.info("getUserIdFrom failed", event, typeof event);
        return false;
    }

    return event.message.chat.id;
}

function getActionFrom(event) {
    //no need to check if message exist because we did it in getUserIdFrom
    console.info("getActionFrom");// got", event);
    if (!event.message.text) {
        console.info("getActionFrom failed", event.message);
        return false
    }

    if (!event.message.text.startsWith('/')) {
        return false;
    }

    const splittedBySpace = event.message.text.split(" ");
    console.info("getActionFrom splittedBySpace", splittedBySpace);

    if (splittedBySpace[0] && splittedBySpace[0].startsWith('/')) {
        //we got an action
        console.info("getActionFrom splittedBySpace", splittedBySpace[0].substr(1));
        return splittedBySpace[0].substr(1);
    }

    return false;
}

async function getPayloadFrom(event) {
    console.info("getPayloadFrom",event);
    //we can get callback or text
    //in getUserIdFrom I moved callback_query to message.text
    if (event.message.text) {
        console.info("getPayloadFrom0");
        if (event.message.text.startsWith('/')) {
            const firstSpace = event.message.text.indexOf(' ');

            //if we have payload
            if (firstSpace >= 0) {
                console.info("getPayloadFrom",
                    event.message.text);
                console.info("getPayloadFrom",
                    event.message.text.
                substr(firstSpace + 1));
                return event.message.text.
                substr(firstSpace + 1);
            }
            //but if we got only command without payload - false
        } else {
            return event.message.text;
        }
    } else {
        //or we can get data like "photo"
        if(event.message.photo && Array.isArray(event.message.photo)) {
            let lastIndex = event.message.photo.length - 1;
            return {type: "photo", fileId: event.message.photo[lastIndex].file_id};
        }
    }

    console.info("getPayloadFrom failed");
    return false;
}

export default async event => {
    console.info("processCommands", typeof event);
    if (wrongEvent(event)) {
        return false;
    }

    if (event.body) event = checkIfEventBodyIsJSON(event);

    let userId = getUserIdFrom(event);

    if (!userId) return false;

    let action = getActionFrom(event);
    console.info("processCommands action:", action);

    const payload = await getPayloadFrom(event);
    console.info("processCommands payload:", payload);

    if (!action && !payload) return false;

    //for user input we pass 'userInput' action
    if (!action) action = 'userInput';

    return {userId, action, payload, event};
};

