import JoiBasic from '@hapi/joi';
import {Profile} from './dbModels/index';
import {sendMessageToTelegram} from './telegramController';

const Joi = JoiBasic.extend(require('joi-phone-number'));

const signUp = Joi.object().keys({
    firstName: Joi.string().alphanum().min(3).max(30)
        .error(new Error("Name must be 3 to 30 characters")),
    lastName: Joi.string().alphanum().min(3).max(30)
        .error(new Error("Name must be 3 to 30 characters")),
    email: Joi.string().email()
        .error(new Error("Email is wrong")),
    phoneNumber: Joi.string().phoneNumber()
        .error(new Error("Wrong phone number")),
    password: Joi.string().min(1).max(50)
        .error(new Error("Password must be minimum 6 characters without spaces")),
    // password_confirmation: Joi.any()
    //     .valid(Joi.ref('password'))
    //     .required()
    //     .error(new Error("Password and confirm password does not match")),
});

async function verifyUserData(name, value, user) {
    console.info("verifyUserData", name, value);
    let checkResult;

    switch(name){
        case 'passwordConfirmation':
            console.info("validateRegistrationInput passwordConfirmation");
            let passwordValidation;

            try {
                passwordValidation = await user.comparePassword(value);
            } catch (err) {
                checkResult = "Passwords doesn't match";
            }
            if (passwordValidation) {
                console.info("Password confirmed");
                checkResult = false;
            } else {
                console.info("Passwords doesn't match");
            }
            break;
        case 'diplomas':
            //we got a file link we must check if it's valid
            // const fileData =
                // await getFileDataFromTelegram(value);
            // console.info(fileData);
        case 'profilePicture':
            if(typeof value !== 'object'
                || value.type !== 'photo') {
                checkResult = "Wrong photo";
            }
            break;
        default:
            checkResult = await defaultValidation(name, value);
            break;
    }

    return checkResult;
}

async function defaultValidation (name, value) {
    const res = signUp.validate({[name]: value});
    console.info("verifyUserData", name, value, "result:", res);
    if (res.error === null) {
        if (name === 'email') {
            const exists = await Profile.findOne({email: value}).exec();
            if (exists) {
                return "Email already registered, use another";
            }
        } else if (name === 'phoneNumber') {
            const exists = await Profile.findOne({phoneNumber: value}).exec();
            if (exists) {
                return "Phone number already registered, use another";
            }
        }
        return false;
    } else {
        return res.error.message;
    }
}

async function sendUserForVerification(user) {
    await sendMessageToTelegram(-1001479295242, `got a new teacher`+
    `${user.profile.firstName} ${user.profile.lastName} ${user.profile.email} ${user.profile.phone}`);
}

export {
    verifyUserData,
    sendUserForVerification
}